package modele;

import java.util.Comparator;

public class AlbumComparator implements Comparator<Album> {
    @Override
    public int compare(Album album, Album t1) {
        int aux = album.getTitle().compareTo(t1.getTitle());

        if (aux != 0)
            return aux;
        return album.getPublishedAt().compareTo(t1.getPublishedAt());
    }
}
