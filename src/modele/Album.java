package modele;

public class Album {

    // C l a s s   A t t r i b u t e
    //TODO

    // I n s t a n c e   A t t r  i b u t e

    private String title;
    private Integer publishedAt;

    // G e t t e r   /   S e t t e r

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = (title == null)
                ? "NA"
                : title.toUpperCase();
    }

    public Integer getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Integer publishedAt) {
        this.publishedAt = Math.max(publishedAt, 1930);
    }

    // B u s i n e s s   R u l e s
    //TODO

    // C o n s t r u c t o r

    public Album(String title, Integer publishedAt) {
        this.setTitle(title);
        this.setPublishedAt(publishedAt);
    }

    public Album() {
        this(null, 0);
    }

    // E q u a l s   /   H a s h c o d e
    //TODO

    // T o S t r i n g

    @Override
    public String toString() {
        return getPublishedAt() + " - " + getTitle();
    }


}
