package modele;

import java.util.*;

public class TintinMania {
// C l a s s   A t t r i b u t e

    private List<Album> myTintin = new ArrayList<Album>();

// I n s t a n c e   A t t r  i b u t e
//TODO

// G e t t e r   /   S e t t e r
//TODO

// B u s i n e s s   R u l e s

    private void loadAlbum() {
        Album a1 = new Album("Tintin au pays des Soviets", 1930);
        Album a2 = new Album("Tintin au Congo", 1931);
        Album a3 = new Album("Tintin en Amérique", 1932);
        Album a4 = new Album("Les Cigares du pharaon", 1934);
        Album a5 = new Album("Le Lotus bleu", 1936);
        Album a6 = new Album("L'Oreille cassée", 1937);
        Album a7 = new Album("L'Ile noire", 1938);
        Album a8 = new Album("Le Sceptre d'Ottokar", 1939);
        Album a9 = new Album("Le Crabe aux pinces d'or", 1941);
        Album a10 = new Album("L'Étoile mystérieuse", 1942);
        Album a11 = new Album("Le Secret de la Licorne", 1943);
        Album a12 = new Album("Le Trésor de Rackham le Rouge", 1944);
        Album a13 = new Album("Les 7 boules de cristal", 1948);
        Album a14 = new Album("Le Temple du soleil", 1950);

        myTintin.add(a1);
        myTintin.add(a2);
        myTintin.add(a3);
        myTintin.add(a4);
        myTintin.add(a5);
        myTintin.add(a6);
        myTintin.add(a7);
        myTintin.add(a8);
        myTintin.add(a9);
        myTintin.add(a10);
        myTintin.add(a11);
        myTintin.add(a12);
        myTintin.add(a13);
        myTintin.add(a14);
    }

    // listeAlbumParAnnee -- listeAlbumParAnneeDecroissant
    public void displayTitleAsc() {
        List<Album> aux = new LinkedList<Album>(this.myTintin);
        aux.sort(new AlbumComparator());
        for (Album p : aux) {
            System.out.println(p);
        }
    }

    public void displayTitleDesc() {
        class AlbumComparatorTitleDesc implements Comparator<Album> {

            @Override
            public int compare(Album album, Album t1) {
                int aux = album.getTitle().compareTo(t1.getTitle());
                if (aux != 0)
                    return -aux;
                else
                    return -album.getPublishedAt().compareTo(t1.getPublishedAt());
            }
        }
        List<Album> aux = new LinkedList<Album>(this.myTintin);
        aux.sort(new AlbumComparatorTitleDesc());
        for (Album p : aux) {
            System.out.println(p);
        }
    }

    public void displayYearAsc() {
        class AlbumComparatorYearAsc implements Comparator<Album> {

            @Override
            public int compare(Album album, Album t1) {
                int aux = -album.getPublishedAt().compareTo(t1.getPublishedAt());
                if (aux != 0)
                    return -aux;
                else
                    return album.getTitle().compareTo(t1.getTitle());
            }
        }
        List<Album> aux = new LinkedList<>(this.myTintin);
        aux.sort(new AlbumComparatorYearAsc());
        for (Album p : aux) {
            System.out.println(p);
        }
    }

    public void displayYearDescAnonymous() {
        List<Album> aux = new LinkedList<>(this.myTintin);
        Collections.sort(aux, new Comparator<Album>() {

            @Override
            public int compare(Album album, Album t1) {
                int aux = album.getPublishedAt().compareTo(t1.getPublishedAt());
                if (aux != 0)
                    return -aux;
                else
                    return album.getTitle().compareTo(t1.getTitle());
            }
        });
        for (Album p : aux) {
            System.out.println(p);
        }
    }
// C o n s t r u c t o r
//TODO

// E q u a l s   /   H a s h c o d e
//TODO

// T o S t r i n g

    @Override
    public String toString() {
        return "TintinMania\n" + myTintin;
    }

    public static void main(String[] args) {
        TintinMania si = new TintinMania();
        si.loadAlbum();
        System.out.println("================By TITLE ASCENDING================");
        si.displayTitleAsc();
        System.out.println("================By TITLE DESCENDING================");
        si.displayTitleDesc();
        System.out.println("================By YEAR ASCENDING================");
        si.displayYearAsc();
        System.out.println("================By YEAR DESCENDING ANONYMOUS================");
        si.displayYearDescAnonymous();
    }
}